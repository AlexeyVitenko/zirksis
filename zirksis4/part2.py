from Crypto.Random import random
from PIL import Image
from numpy import array
from numpy import zeros
import sys

#modify alpha channel
CHANNEL = 3 
N = 20000
SIGMA = 3

def access(size, i):
    return i/size[0], i%size[0]

def rand_access(size, start, p_count):
    return access(size, int(random.randint(start, (start + p_count/2)-1)))

def check(pixels, size):
    count = size[0] * size[1]
    summ = 0
    for i in range(count/2):
        a = access(size, i)
        b = access(size, count/2 + i)
        try:
            summ += pixels[a][CHANNEL] - pixels[b][CHANNEL]
        except IndexError:
            print a, b
    print summ

def modify(pixels, size):
    count = size[0] * size[1]
    for i in range(N):
        a = rand_access(size, 0, count)
        b = rand_access(size, count/2, count)
        try:
            ap = list(pixels[a])
            bp = list(pixels[b])
            ap[CHANNEL] += SIGMA
            bp[CHANNEL] -= SIGMA
            pixels[a] = tuple(ap)
            pixels[b] = tuple(bp)
        except IndexError:
            print a, b
            

img = Image.open(sys.argv[1])
size = img.size
p_count = size[0] * size[1]
pixels = img.load()
if '-m' in sys.argv:
    modify(pixels, size)
    img.save('out'+sys.argv[1])
check(pixels, size)
