#ifndef _COMMONIO_

#define _COMMONIO_

#include "stdafx.h"

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

DWORD readFile(LPCTSTR fname, byte ** data);
DWORD writeFile(LPCTSTR fname, byte * data, int size); 

BYTE * wideStringToByteArray(LPCWSTR string, DWORD * dwArrayLength);

#endif /*_COMMONIO_*/