// zirksis2.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"
#include "zirksis.h"

#define _UNICODE
#define VERIFY_HASH
#define RSA_SIGN
#define HMAC_KEY_LENGTH 16

HANDLE hStdOut;

int _tmain(int argc, _TCHAR* argv[])
{
	
	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	AttachConsole(ATTACH_PARENT_PROCESS);
#ifdef RSA_SIGN
#ifdef VERIFY_HASH
	if (verifyRSA(L"1.doc", L"1.sign")) {
		WriteConsole(hStdOut, L"Valid", 6, 0, 0);
	} else {
		WriteConsole(hStdOut, L"Invalid", 8, 0, 0);
	}
#else
	signRSA(L"1.doc", L"1.sign", L"��blablakey");
#endif
#else
#ifdef VERIFY_HASH
	if (verifyHMAC(L"1s.doc", L"��blablakey")) {
		WriteConsole(hStdOut, L"Valid", 6, 0, 0);
	} else {
		WriteConsole(hStdOut, L"Invalid", 8, 0, 0);
	}
#else
	signHMAC(L"1.doc", L"1s.doc", L"��blablakey");
#endif
#endif
	CloseHandle(hStdOut);
	return 0;
}

void signHMAC(LPCTSTR fileName, LPCTSTR outFileName, LPCWSTR key) {
	BYTE * bData;
	DWORD dwHashLength;
	DWORD dwFileLen = readFile(fileName, &bData);


	BYTE * hash = createHMAC(key, bData, dwFileLen, &dwHashLength);
	DWORD dwNewSize = dwFileLen + dwHashLength;

	BYTE * newData = (BYTE*)malloc(dwNewSize * sizeof(BYTE));
	memcpy(newData, bData, dwFileLen);
	memcpy(&newData[dwFileLen], hash, dwHashLength);
	writeFile(outFileName, newData, dwNewSize);
	
	free(newData);
	free(bData);
	free(hash);
}

BOOL verifyHMAC(LPCTSTR fileName, LPCTSTR key) {
	BYTE * bData;
	DWORD dwHashLength;
	DWORD dwFileLen = readFile(fileName, &bData);

	BYTE * hash = createHMAC(key, bData, dwFileLen - 16, &dwHashLength);
	BYTE * old = (BYTE *)malloc(sizeof(BYTE) * 16);
	memcpy(old, &bData[dwFileLen - 16], 16);
	BOOL result = TRUE;
	if (memcmp(hash, old, 16) == 0) {
		result = TRUE;
	} else {
		result = FALSE;
	}
	free(old);
	free(bData);
	free(hash);
	return result;
}

BYTE * createHMAC(LPCWSTR key, BYTE * bData, DWORD dwFileLen, DWORD * dwHashLength) {
	DWORD dwKeyLength;
	BYTE * k = wideStringToByteArray(key, &dwKeyLength);
	
	if (dwKeyLength < HMAC_KEY_LENGTH) {
		BYTE * temp = (BYTE *)malloc(16);
		memcpy(&temp[HMAC_KEY_LENGTH - dwKeyLength], k, dwKeyLength);
		memset(temp, 0, HMAC_KEY_LENGTH - dwKeyLength);
		free(k);
		dwKeyLength = 16;
		k = temp;
	} else {
		BYTE * temp = (BYTE *)malloc(16);
		memcpy(temp, k, dwKeyLength);
		free(k);
		dwKeyLength = 16;
		k = temp;
	}	

	DWORD dwNewSize = dwFileLen+dwKeyLength;

	BYTE * tempKey = (BYTE *)malloc(dwKeyLength);
	memcpy(tempKey, k, dwKeyLength);
	//ipad
	for (int i=0; i < dwKeyLength; i++) {
		tempKey[i] = tempKey[i] ^ 0x36;
	}

	BYTE * m = (BYTE*)malloc(dwNewSize * sizeof(BYTE));
	memcpy(m, tempKey, dwKeyLength);
	memcpy(&m[dwKeyLength], bData, dwFileLen);

	BYTE * hash = createHash(m, dwNewSize, dwHashLength);

	memcpy(tempKey, k, dwKeyLength);
	//opad
	for (int i=0; i < dwKeyLength; i++) {
		tempKey[i] = tempKey[i] ^ 0x5C;
	}

	BYTE * lastHash = (BYTE *)malloc(dwKeyLength + *dwHashLength);
	memcpy(lastHash, tempKey, dwKeyLength);
	memcpy(&lastHash[dwKeyLength], hash, *dwHashLength);
	
	hash = createHash(lastHash, dwKeyLength + *dwHashLength, dwHashLength);
	
	free(m);
	free(lastHash);
//	free(k);
	free(tempKey);
	return hash;
}

BYTE * createHash(BYTE * data, DWORD dwLength, DWORD * dwHashLength) {
	HCRYPTPROV hProv;
	CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, NULL);

	HCRYPTHASH hHash;
	CryptCreateHash(hProv, CALG_MD5, NULL, NULL, &hHash);
	CryptHashData(hHash, data, dwLength, NULL);
	BYTE * newHash = (BYTE *)malloc(16* sizeof(BYTE));
	*dwHashLength = 16;

	CryptGetHashParam(hHash, HP_HASHVAL, newHash, dwHashLength, 0);

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, NULL);
	return newHash;
}

void signRSA(LPCTSTR fileName, LPCTSTR signName){
	BYTE * bData;
	DWORD dwFileLen = readFile(fileName, &bData);
	
	HCRYPTPROV hProv;
	CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, NULL);

	HCRYPTHASH hHash;
	CryptCreateHash(hProv, CALG_MD5, NULL, NULL, &hHash);
	CryptHashData(hHash, bData, dwFileLen, NULL);
	int e = GetLastError();
	BYTE * newHash = (BYTE *)malloc(16* sizeof(BYTE));
	DWORD dwHashLength = 16;

	CryptGetHashParam(hHash, HP_HASHVAL, newHash, &dwHashLength, 0);
	e = GetLastError();
	LPTSTR szDescription = TEXT("Test Data");
	
	HCRYPTKEY hKey;
	HCRYPTKEY hUserKey;
	//CryptGenKey(hProv, CALG_RSA_SIGN, NULL, &hKey);
	//e = GetLastError();
	//CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hUserKey);
	//e = GetLastError();

	DWORD dwSigLen= 0;
	CryptSignHash(hHash, AT_KEYEXCHANGE, TEXT(""), 0, NULL, &dwSigLen);
	e = GetLastError();
	BYTE * pbSignature = (byte *)malloc(dwSigLen);
	// Sign the hash object.
	if(!CryptSignHash(hHash, AT_KEYEXCHANGE, TEXT(""), 0, pbSignature, 
	 &dwSigLen)) {
	}	
	e = GetLastError();
	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, NULL);

	writeFile(signName, pbSignature, dwSigLen);
	
	free(pbSignature);
	free(bData);
	free(newHash);	
}


BOOL verifyRSA(LPCTSTR fileName, LPCTSTR signName) {
	BYTE * bData;
	DWORD dwFileLen = readFile(fileName, &bData);
	
	HCRYPTPROV hProv;
	CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, NULL);

	HCRYPTHASH hHash;
	CryptCreateHash(hProv, CALG_MD5, NULL, NULL, &hHash);
	CryptHashData(hHash, bData, dwFileLen, NULL);
	int e = GetLastError();
	BYTE * newHash = (BYTE *)malloc(16* sizeof(BYTE));
	DWORD dwHashLength = 16;

	CryptGetHashParam(hHash, HP_HASHVAL, newHash, &dwHashLength, 0);
	e = GetLastError();
	LPTSTR szDescription = TEXT("Test Data");
	
	BYTE * pbSignature;
	DWORD dwSigLen = readFile(signName, &pbSignature);

	HCRYPTKEY hUserKey;

	CryptGetUserKey(hProv, AT_SIGNATURE, &hUserKey);

	if(!CryptVerifySignature(hHash, pbSignature, dwSigLen, hUserKey, 
	 szDescription, 0)) {
	 if(GetLastError() == NTE_BAD_SIGNATURE) {
	 printf("Signature not validated!\n");
	 } else {
	 printf("Error %x during CryptVerifySignature!\n", 
	 GetLastError());
	 }
	} else {
	 printf("Signature validated\n");
	}


	free(pbSignature);
	free(bData);
	free(newHash);	
	return true;
}