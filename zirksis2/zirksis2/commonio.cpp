#include "stdafx.h"
#include "commonio.h"

DWORD readFile(LPCTSTR fname, byte ** data){
	int workingDirBuffLen = 2000;
	TCHAR * buffer = (TCHAR *) malloc(workingDirBuffLen * sizeof(TCHAR));
	GetFullPathName(fname, workingDirBuffLen, buffer, NULL);
	HANDLE hFile = CreateFile(buffer, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD bRead = -1;

	if (! (hFile == INVALID_HANDLE_VALUE)) {
		int size = GetFileSize(hFile, NULL);
		*data = (byte*) malloc(size);
		ReadFile(hFile, *data, size, &bRead, NULL);
	}
	free(buffer);
	CloseHandle(hFile);
	return bRead;
}

DWORD writeFile(LPCTSTR fname, byte * data, int size) {
	HANDLE hFile = CreateFile(fname, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD writen = -1;
	if (!(hFile == INVALID_HANDLE_VALUE)) {
		WriteFile(hFile, data, size, &writen, 0);
	}

	CloseHandle(hFile);
	return writen;
}

BYTE * wideStringToByteArray(LPCWSTR string, DWORD * dwArrayLength) {
	UINT16 lowest = 0xFF;
	UINT16 highest = 0xFF00;
	DWORD strLen = wcslen(string);
	*dwArrayLength = strLen*2;
	BYTE * arr = (BYTE *)malloc(*dwArrayLength);
	
	for (int i = 0; i<strLen; i++) {
		INT16 nextChar = string[i];
		arr[i*2] = (nextChar & highest) >> 8;
		arr[i*2 + 1] = nextChar & lowest;
	}
	return arr;
}