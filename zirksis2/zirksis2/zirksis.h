#include "stdafx.h"
#include <wincrypt.h>
#include "commonio.h"

void signHMAC(LPCTSTR fileName, LPCTSTR outFileName, LPCTSTR key);
BOOL verifyHMAC(LPCTSTR fileName, LPCTSTR key);
BYTE * createHMAC(LPCWSTR key, BYTE * bData, DWORD dwFileLen, DWORD * dwHashLength);
BYTE * createHash(BYTE * data, DWORD dwLength, DWORD * dwHashLength);
void signRSA(LPCTSTR fileName, LPCTSTR signName);
BOOL verifyRSA(LPCTSTR fileName, LPCTSTR signName);